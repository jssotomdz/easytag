<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="guide" id="file-rename" xml:lang="cs">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-08-18" status="draft"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak změnit název souboru pomocí štítku</desc>
  </info>

  <title>Přejmenování souborů</title>

  <p>Přímo v aplikaci <app>EasyTAG</app> můžete své hudební soubory přejmenovat pomocí štítků:</p>

  <steps>
    <item>
      <p><link xref="file-select">Vyberte soubory</link>, které si přejete v zobrazení souborů přejmenovat.</p>
    </item>
    <item>
      <p>Vyberte <guiseq><gui style="menu">Zobrazit</gui> <gui style="menuitem">Zobrazit průzkumníka</gui></guiseq>.</p>
    </item>
    <item>
      <p>Vyberte průzkumníka <gui>Přejmenovat soubory</gui>.</p>
    </item>
    <item>
      <p>Podle <gui xref="format-specifier">legendy</gui> zapište strukturu názvu souboru, aby odpovídala tomu, jak chcete soubor pojmenovat. Když například chcete název ve tvaru <file>[umělec] - [název].[přípona]</file>, zadejte do pole pro přejmenování <input>%a - %t</input></p>
    </item>
    <item>
      <p>Pro použití změn u vybraných souborů klikněte na <gui style="button">Prozkoumat soubory</gui>.</p>
    </item>
    <item>
      <p>Zavřete dialogové okno <gui style="dialog">Průzkum štítků a názvů souborů</gui>.</p>
    </item>
    <item>
      <p>Aby se použité změny uložily, vyberte <guiseq><gui style="menu">Soubor</gui> <gui style="menuitem">Uložit soubory</gui></guiseq>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Pokud aplikaci zavřete bez uložení, budou vaše změny ztraceny.</p>
  </note>

</page>
