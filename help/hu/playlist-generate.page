<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="playlist-generate" xml:lang="hu">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lejátszólista készítése a fájllistából</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at ubuntu dot com</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

  <title>Lejátszólista létrehozása</title>

  <p>Létrehozhat egy zenelejátszókban használható M3U lejátszólistát az <app>EasyTAG-ben</app> megjelenített fájllista alapján:</p>

  <steps>
    <item>
      <p><link xref="file-select">Jelölje ki</link> a fájlnézetben a lejátszólistára felvenni kívánt fájlokat.</p>
    </item>
    <item>
      <p>Válassza az <guiseq><gui style="menu">Egyéb</gui><gui style="menuitem">Lejátszólista előállítása</gui></guiseq> menüpontot.</p>
    </item>
    <item>
      <p>Válassza a <gui style="radiobutton">Használandó maszk</gui> lehetőséget, és írjon be egy maszkot <link xref="format-specifier">formátumleírók</link> használatával, vagy válassza a <gui style="radiobutton">Könyvtár nevének használata</gui> lehetőséget a lejátszólista elnevezéséhez a <gui style="group">Böngészőben</gui> kiválasztott könyvtár alapján.</p>
    </item>
    <item>
      <p>Jelölje be a <gui style="checkbox">Csak a kijelölt fájlok felvétele</gui> négyzetet a fájllistában kijelölt fájlok felvételéhez a lejátszólista előállításakor. Törölje az összes megjelenített fájl felvételéhez a lejátszólistára.</p>
    </item>
    <item>
      <p>Válassza a <gui style="radiobutton">Relatív elérési út használata a lejátszólistában</gui> lehetőséget, hacsak nem kizárólag az aktuális számítógépen tervezi használni a lejátszólistát, és nem tervezi a hangfájlok áthelyezését sem. Egyébként a <gui style="radiobutton">Teljes elérési út használata a lejátszólistában</gui> lehetőséget válassza.</p>
    </item>
    <item>
      <p>Jelölje be a <gui style="checkbox">Lejátszólista létrehozása a szülőkönyvtárban</gui> négyzetet, ha a lejátszólistát a <gui style="group">Böngészőben</gui> kiválasztott könyvtár szülőkönyvtárába szeretné menteni. Egyébként a lejátszólista a <gui style="group">Böngészőben</gui> kiválasztott könyvtárba jön létre.</p>
    </item>
    <item>
      <p>Ha a lejátszólistát egy windowsos számítógépen, vagy  <sys>NTFS</sys> vagy <sys>FAT</sys> fájlrendszeren szeretné használni, akkor jelölje be a <gui style="checkbox">DOS könyvtárelválasztó használata</gui> négyzetet.</p>
    </item>
    <item>
      <p>Válassza a <gui style="radiobutton">Csak fájlok listájának írása</gui> lehetőséget egy csak fájlnevek listáját tartalmazó lejátszólista létrehozásához. Válassza az <gui style="radiobutton">Információk írása a fájlnév használatával</gui> lehetőséget bővített információk kiírásához a lejátszólistába, beleértve a hangfájl hosszát is. Egyéni bővített információk lejátszólistába írásához válassza az <gui style="radiobutton">Információk írása ezek használatával</gui> lehetőséget, és formátumleírók használatával adjon meg egy maszkot.</p>
    </item>
    <item>
      <p>A lejátszólista előállításához nyomja meg a <gui style="button">Mentés</gui> gombot.</p>
    </item>
  </steps>

</page>
