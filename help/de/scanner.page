<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="scanner" xml:lang="de">

  <info>
    <link type="guide" xref="index#main" group="#first"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="review"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Metainformationen automatisch aktualisieren</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Der <gui>Scanner</gui></title>

  <p>Der Scanner kann zum Ausfüllen der Felder basierend auf dem Dateinamen, zum Umbenennen einer Datei, zum Anlegen eines neuen Ordners basierend auf den ausgefüllten Feldern und zum Verarbeiten des Texts in den Feldern und dem Dateinamen verwendet werden.</p>

<section id="fill-tag">
  <title>Felder anhand der Dateinamen und Orderstruktur befüllen</title>

  <p><gui>Metainformationen füllen</gui> kann zum Befüllen der Metainfofelder basierend auf den Dateinamen und den übergeordneten Ordnern verwendet werden.</p>

  <p>Mit <em xref="format-specifier">Formatbezeichnern</em> und <em>Trennern</em> teilen Sie den Dateinamen und Elternordner in unterschiedliche Metadaten-Felder. Die <em xref="format-specifier">Formatbezeichner</em> wie <input>%t</input> für den Titel des Stücks, die die unterschiedlichen Metadaten-Felder bezeichnen, werden in der <gui>Legende</gui> aufgelistet. <em>Trenner</em> können an beliebiger Stelle im Dateinamen oder den Elternordnern stehen. Verwenden Sie <key>/</key> als <em>Trenner</em> für einen Elternordner.</p>

  <p>Wenn Sie beispielsweise für Ihre Audio-Dateien eine Verzeichnis- und Dateinamensstruktur der Form <file>Künstler/Album/01 Stück Titel.flac</file> verwenden, können Sie mit der <em>Formatzeichenkette</em> <input>%a/%b/%n %t</input> die Stücknummer und den Titel aus dem Dateinamen erzeugen, den Albentitel aus dem Elternordner und den Künstler aus dem Großelternordner.</p>

  <p>Befüllen Sie die Metainfofelder der ausgewählten Dateien durch Anklicken des Knopfes <gui style="button">Dateien einlesen</gui>.</p>

</section>

<section id="rename">
  <title>Dateien umbenennen und neue Ordner anlegen</title>

  <p>Mit <gui xref="file-rename">Datei umbenennen</gui> legen Sie eine Ordnerhierarchie an und aktualisieren die Dateinamen Dateinamen anhand der ausgefüllten Metadaten-Felder. Wenn eine neue Ordnerhierarchie angegeben wird, wird diese innerhalb des gleichen Ordners angelegt, in dem sich die aktuelle Datei befindet.</p>

  <p>Beispielsweise haben Sie eine Datei innerhalb des Ordners <file>Musik</file> mit Tags versehen. Dann können Sie die <em>Formatzeichenkette</em> <input>%a/%b/%n %t</input> verwenden, um Dateistruktur und -name der Form <file>Musik/Künstler/Album/01 Stück Titel.flac</file> zu erzeugen. Sie sehen eine Vorschau des Benennungsschemas unterhalb der angegebenen Formatzeichenkette.</p>

  <p>Um Dateien zum Verschieben und Umbenennen vorzubereiten, klicken Sie auf den Knopf <gui style="button">Dateien einlesen</gui>.</p>

</section>

<section id="process">
  <title>Stapelverarbeitung von Metadaten-Feldern und Dateinamen</title>

  <p><gui>Felder verarbeiten</gui> ist ein anspruchsvolles Funktionsmerkmal zum Suchen und Ersetzen, mit dem Sie wählen können, welche Felder verarbeitet werden können. Auch der Dateiname kann verarbeitet werden.</p>

  <p>Geben Sie an, welche Felder geändert werden sollen, indem Sie diese im Abschnitt <gui>Felder füllen</gui> auswählen.</p>

  <p>Für einfaches Suchen und Ersetzen können Sie Zeichen <gui>umwandeln</gui>, die Groß- oder Kleinschreibung ändern und Leerzeichen hinzufügen oder entfernen.</p>

</section>

</page>
