<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tag-capitalization" xml:lang="de">

  <info>
    <link type="seealso" xref="scanner"/>
    <revision pkgversion="2.3.5" date="2015-03-26" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Konsistente Großschreibung auf Metainformationen anwenden</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Metainformationen konsistent groß schreiben</title>

  <p>Die Groß- oder Kleinschreibung kann für eines oder mehrere Metainformations-Felder in einer oder mehreren Dateien geändert werden.</p>

  <steps>
    <title>Schreibung der Metainformations-Felder ändern</title>
    <item>
      <p>Wählen Sie die Dateien, die Sie bearbeiten möchten.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Ansicht</gui> <gui style="menuitem">Scanner anzeigen</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie den Reiter <gui style="tab">Felder verarbeiten</gui></p>
    </item>
    <item>
      <p>Wählen Sie die Felder, die Sie ändern wollen.</p>
    </item>
    <item>
      <p>Für die Änderung der Schreibweise ist eine Reihe von Optionen verfügbar. Wählen Sie die gewünschte Option aus.</p>
    </item>
    <item>
      <p>Um die Änderungen auf die gewählten Dateien anzuwenden, klicken Sie auf <gui style="button">Dateien einlesen</gui>.</p>
    </item>
  </steps>

  <p>Im <gui>Scanner</gui>-Dialog können Sie außerdem die Verarbeitung von Leerzeichen und Umwandlung von Zeichen anpassen. Wenn Sie hier nichts verändern wollen, wählen Sie die korrekte Option, bevor Sie <gui style="button">Dateien einlesen</gui> anklicken.</p>

</page>
