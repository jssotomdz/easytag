<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="guide" id="playlist-generate" xml:lang="fr">

  <info>
    <link type="guide" xref="index#main"/>
    <revision pkgversion="2.1.9" date="2013-10-06" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Générer une liste de lecture à partir de la liste de fichiers</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Création d'une liste de lecture</title>

  <p>Vous pouvez créer une liste de lecture M3U à utiliser avec un lecteur de musique, fondée sur la liste de fichiers affichés dans <app>EasyTAG</app> :</p>

  <steps>
    <item>
      <p>Dans la liste des fichiers, <link xref="file-select">sélectionnez les fichiers</link> que vous voulez voir figurer dans la liste de lecture.</p>
    </item>
    <item>
      <p>Choisissez <guiseq><gui style="menu">Divers</gui> <gui style="menuitem">Générer une liste de lecture…</gui></guiseq>.</p>
    </item>
    <item>
      <p>Sélectionnez <gui style="radiobutton">Utiliser un masque</gui> et saisissez un masque, en vous servant des <link xref="format-specifier">Spécificateurs de format</link>, ou sélectionnez <gui style="radiobutton">Utiliser le nom du répertoire</gui> pour nommer la liste de lecture d'après le répertoire sélectionné dans le <gui style="group">Sélecteur</gui>.</p>
    </item>
    <item>
      <p>Sélectionnez <gui style="checkbox">Inclure seulement les fichiers sélectionnés</gui> pour employer les fichiers sélectionnés dans la liste de fichiers lors de la génération de la liste de lecture. Désactivez cette option pour inclure tous les fichiers figurant dans la liste de lecture.</p>
    </item>
    <item>
      <p>Activez <gui style="radiobutton">Utiliser le chemin relatif pour les fichiers de la liste de lecture</gui> si vous n'envisagez d'utiliser la liste de lecture que sur le même ordinateur et que vous ne prévoyez pas de déplacer les fichiers audio. Sinon, activez <gui style="radiobutton">Utiliser le chemin absolu pour les fichiers de la liste de lecture</gui>.</p>
    </item>
    <item>
      <p>Sélectionnez <gui style="checkbox">Créer la liste de lecture dans le répertoire parent</gui> si vous voulez enregistrer la liste de lecture dans le parent du répertoire sélectionné dans le <gui style="group">Sélecteur</gui>. Sinon, la liste de lecture sera enregistrée dans le répertoire sélectionné dans le <gui style="group">Sélecteur</gui>.</p>
    </item>
    <item>
      <p>Si vous créez une liste de lecture à utiliser sur un ordinateur Windows, ou sur un système de fichiers <sys>NTFS</sys> ou <sys>FAT</sys>, sélectionnez <gui style="checkbox">Utiliser le séparateur de répertoires DOS</gui>.</p>
    </item>
    <item>
      <p>Activez <gui style="radiobutton">Écrire seulement la liste des fichiers</gui> pour créer une liste de lecture ne contenant qu'une liste de fichiers. Activez <gui style="radiobutton">Écrire les informations en utilisant le nom du fichier</gui> pour inscrire des informations étendues, comprenant la durée du fichier audio, dans la liste de lecture. Activez <gui style="radiobutton">Écrire les informations en utilisant :</gui> et saisissez un masque en vous servant des spécificateurs de format, pour inscrire des informations étendues personnalisées dans la liste de lecture.</p>
    </item>
    <item>
      <p>Pour générer la liste de lecture, cliquez sur le bouton <gui style="button">Enregistrer</gui>.</p>
    </item>
  </steps>

</page>
