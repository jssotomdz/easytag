Source: easytag
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Reinhard Tartler <siretart@tauware.de>,
 James Cowgill <jcowgill@debian.org>,
 David King <amigadave@amigadave.com>
Build-Depends:
 appstream-util (>= 0.7.2),
 debhelper-compat (= 11),
 desktop-file-utils,
 docbook-xml,
 docbook-xsl,
 gettext,
 intltool (>= 0.50.0),
 libflac-dev,
 libglib2.0-dev (>= 2.38),
 libgtk-3-dev (>= 3.10),
 libid3-3.8.3-dev,
 libid3tag0-dev,
 libogg-dev,
 libopusfile-dev,
 libspeex-dev,
 libtag1-dev,
 libvorbis-dev,
 libwavpack-dev,
 yelp-tools,
 zlib1g-dev
Standards-Version: 4.1.3
Homepage: https://wiki.gnome.org/Apps/EasyTAG
Vcs-Git: https://salsa.debian.org/multimedia-team/easytag.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/easytag

Package: easytag
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 gnome-icon-theme,
 gvfs,
 yelp
Description: GTK+ editor for audio file tags
 EasyTAG is an utility for viewing, editing and writing
 the tags of different audio files, using a GTK+ interface.
 .
 Currently EasyTAG supports the following:
  - View, edit, write tags of MP3, MP2 files (ID3 tag), FLAC files (FLAC Vorbis
    tag), Ogg Opus, Ogg Speex and Ogg Vorbis files (Ogg Vorbis tag),
    MP4/M4A/AAC files (MPEG-4 Part 10 tag), and MusePack, Monkey's Audio files
    (APE tag);
  - Auto tagging: parse file and directory names using masks to automatically
    fill in tag fields;
  - Cover art support for all formats;
  - Rename files from the tag fields (using masks) or by loading a text file;
  - Process selected files of the selected directory;
  - Ability to browse subdirectories;
  - Recursion for tagging, removing, renaming, saving, etc;
  - Can set a field (artist, title, ...) on all other selected files;
  - Read file header information (bitrate, time, ...) and display it;
  - Undo and redo last changes;
  - Ability to process tag fields and file names (convert letters into
    uppercase, lowercase, etc);
  - Ability to open a directory or a file with an external program;
  - CDDB support (from http protocol);
  - A tree based browser;
  - A list to select files;
  - A playlist generator window;
  - A file searching window;
  - Simple and explicit interface.
